<?php

namespace App\Http\Controllers;

use App\Insider\League;
use App\Team;

class MatchController extends Controller
{
    public function index(League $league)
    {
        return [
            'matchResults' => $league->startWeekGames()->weekResults,
            'teams' => Team::all()
        ];
    }
}
