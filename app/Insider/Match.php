<?php

namespace App\Insider;

use App\League;
use App\Team;

class Match
{

    public function start($teams)
    {
        $home = $teams[0];
        $away = $teams[1];

        $average_win_home = $home->wins ? $home->games / $home->wins : 0;
        $average_win_away = $away->wins ? $away->games / $away->wins : 0;


        $average_defeat_home = $home->defeats ? $home->games / $home->defeats : 0;
        $average_defeat_away = $away->defeats ? $away->games / $away->defeats : 0;

        $average_draw_home = $home->draws ? $home->games / $home->draws : 0;
        $average_draw_away = $away->draws ? $away->games / $away->draws : 0;


        $average_goal_home = $home->goals ? $home->games / $home->goals: 0;
        $average_goal_away = $away->goals ? $away->games / $away->goals: 0;


        $homeWin = ($average_win_home + $average_draw_home + $average_defeat_away) / 3;
        $awayWin = ($average_win_away + $average_draw_away + $average_defeat_home) / 3;

        if($homeWin > $awayWin)
        {
            $scoreHome = ceil($average_goal_home + $homeWin);
            $scoreAway = floor($average_goal_away + $awayWin);

        } elseif($homeWin < $awayWin) {
            $scoreHome = floor($average_goal_home + $homeWin);
            $scoreAway = ceil($average_goal_away + $awayWin);
        } else {
            $scoreHome = round($average_goal_home + $homeWin);
            $scoreAway = round($average_goal_away + $awayWin);
        }
        $home->score = $scoreHome;
        $away->score = $scoreAway;
        $this->updateStats($home, $scoreHome, $scoreAway, $away);
        $result = [
            $home,
            $away
        ];
        shuffle($result);
        return $result;
    }

    /**
     * @param $home
     * @param float $scoreHome
     * @param float $scoreAway
     * @param $away
     */
    public function updateStats($home, float $scoreHome, float $scoreAway, $away): void
    {
        $homeTeam = Team::find($home->id);
        $homeTeam->increment('games');
        $homeTeam->increment('goals', $scoreHome);
        if ($scoreHome > $scoreAway) {
            $homeTeam->increment('points', 3);
            $homeTeam->increment('wins');
        }


        $awayTeam = Team::find($away->id);
        $awayTeam->increment('games');
        $awayTeam->increment('goals', $scoreAway);
        if ($scoreHome < $scoreAway) {
            $awayTeam->increment('points', 3);
            $awayTeam->increment('wins');
        }
    }
}