<?php

namespace App\Insider;


use App\Team;
use Illuminate\Support\Facades\DB;

class League
{
    /**
     * @var Match
     */
    private $match;

    public $weekResults = [];

    /**
     * League constructor.
     * @param Match $match
     */
    public function __construct(Match $match)
    {
        $this->match = $match;
    }

    public function startWeekGames($week = 1)
    {
        DB::table('teams')->orderBy('id', 'desc')->chunk(2, function($teams) {
            $this->weekResults[] = $this->match->start($teams);
        });

        return $this;
    }
}