<?php

use App\Team;
use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::create(
        [
            'name' => 'Arsenal',
            'points' => 3,
            'wins' => 1,
            'goals' => 2
        ]);
        Team::create(
        [
            'name' => 'Manchester City',
            'defeats' => 1,
            'goals' => 1
        ]);
        Team::create(
        [
            'name' => 'Liverpool',
            'points' => 3,
            'wins' => 1,
            'goals' => 4
        ]);
        Team::create(
        [
            'name' => 'Chelsea',
            'defeats' => 1,
            'goals' => 0
        ]);
    }
}
